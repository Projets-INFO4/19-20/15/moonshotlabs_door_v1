import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OpenDoorComponent } from './open-door/open-door.component';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  { path: 'open-door', component: OpenDoorComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    OpenDoorComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes,{enableTracing: true}),
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

